var config = {
    "map": {
        "*": {
            "Scrollbar": "Kowal_Scrollbar/js/scrollbar"
        }
    },
    "shim":{
        "Kowal_Scrollbar/js/jquery.mCustomScrollbar.min": ["jquery"],
        "Kowal_Scrollbar/js/jquery.mousewheel.min": ["jquery"]
    }
};

