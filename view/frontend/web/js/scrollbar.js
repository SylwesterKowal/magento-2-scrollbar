define([
    "jquery",
    "Kowal_Scrollbar/js/jquery.mCustomScrollbar.min",
    "Kowal_Scrollbar/js/jquery.mousewheel.min",
], function($){
    return function (config, element) {
        return $(element).mCustomScrollbar(config);
    }
});